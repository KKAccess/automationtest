Feature: 86400 Hiring Test

  #UI Automation on eBay
  @UI @Regression
  Scenario: Search a product on eBay
    Given user opens browser and navigate to "https://www.ebay.com.au" website
    When user search for item "Switch"
    Then matched results will be listed on the result page

  #REST API testing
  @API @Regression
  Scenario Outline: REST API
    When user send the request to the endpoint "<endpoint>", method "<method>", query parameter "<parameter>"
    Then user can get server response with status code "<code>"

  Examples:
  | endpoint | method | parameter | code |
  | https://petstore.swagger.io/v2/pet | Get | 1 | 200 |
  | https://petstore.swagger.io/v2/pet | Put | success | 200 |
  | https://petstore.swagger.io/v2/pet | Delete | 3 | 200 |
