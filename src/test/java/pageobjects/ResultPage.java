package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ResultPage {
    private WebDriver driver;

    private By bestMatchList = By.xpath("//span[contains(text(),'Best Match')]");
    private By NumItemFound = By.cssSelector("h1.srp-controls__count-heading > span:nth-child(1)");

    public ResultPage(WebDriver driver) {
        this.driver = driver;
    }

    public boolean isBestMatchListPresence() {
        return driver.findElement(bestMatchList).isDisplayed();
    }

    public int NumberOfItemFound() {
        if(driver.findElement(NumItemFound).isDisplayed()) {
            return Integer.parseInt(driver.findElement(NumItemFound).getText().trim().replaceAll(",",""));
        }
        else {
            return 0;
        }
    }

    public String getTitle() {
        return driver.getTitle();
    }

}
