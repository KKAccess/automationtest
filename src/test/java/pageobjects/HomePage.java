package pageobjects;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class HomePage {

    private WebDriver driver;

    private By searchBox = By.cssSelector("#gh-ac");
    private By searchButton = By.cssSelector("#gh-btn");

    public HomePage(WebDriver driver) {
        this.driver = driver;
    }

    public void searchByText(String keywords) {
        driver.findElement(searchBox).sendKeys(keywords);
        driver.findElement(searchButton).click();
    }

    public String getTitle() {
        return driver.getTitle();
    }

}
