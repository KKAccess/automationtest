package step_definitions;

import io.cucumber.java.en.*;
import io.github.bonigarcia.wdm.WebDriverManager;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.json.simple.JSONObject;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pageobjects.HomePage;
import pageobjects.ResultPage;
import java.util.concurrent.TimeUnit;
import static io.restassured.RestAssured.given;

public class Steps {

    private WebDriver driver;
    private HomePage homePg;
    private ResultPage resultPg;
    private String strItem;
    private Response response;

    @Given("user opens browser and navigate to {string} website")
    public void user_opens_browser_and_navigate_to_website(String url) {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10000, TimeUnit.MILLISECONDS);
        driver.manage().window().maximize();
        driver.get(url);

        homePg = new HomePage(driver);

    }

    @When("user search for item {string}")
    public void user_search_for_item(String searchText) {
        this.strItem = searchText;
        Assert.assertEquals("Electronics, Cars, Fashion, Collectibles & More | eBay", homePg.getTitle());
        homePg.searchByText(searchText);
    }

    @Then("matched results will be listed on the result page")
    public void matched_results_will_be_listed_on_the_result_page() {
        resultPg = new ResultPage(driver);
        Assert.assertTrue(resultPg.isBestMatchListPresence());
        System.out.printf("%d items found for \"%s\"\n", resultPg.NumberOfItemFound(), strItem);
        driver.quit();
    }



    @When("user send the request to the endpoint {string}, method {string}, query parameter {string}")
    public void user_send_the_request_to_the_endpoint_method_query_parameter(String endpoint, String method, String param) {

        if(method.equalsIgnoreCase("get")) {
            String url = endpoint + "/" + param;
            response = RestAssured.get(url);
        }
        else if(method.equalsIgnoreCase("post")) {
            //not required
        }
        else if(method.equalsIgnoreCase("put")) {
            JSONObject payload = new JSONObject();
            payload.put("id",0);
            payload.put("name",param);
            System.out.println(payload.toJSONString());
            response = given().header("Content-Type","application/json").contentType(ContentType.JSON).body(payload.toJSONString()).put(endpoint);
        }
        else if(method.equalsIgnoreCase("delete")) {
            String url = endpoint + "/" + param;
            response = RestAssured.delete(url);
        }
    }

    @Then("user can get server response with status code {string}")
    public void user_can_get_server_response_with_code(String expectedCode) {
        System.out.println("status code: " + response.getStatusCode());
        Assert.assertEquals(expectedCode,Integer.toString(response.getStatusCode()));
    }

}
